#!/bin/bash
# refreshes the public openbsd keys using their CVS server

set -eu

here=$(readlink -f $(dirname $0))
tmp=$(mktemp -d)

cd $tmp
echo "VERIFY THE SERVER FINGERPRINT WITH THE LIST BELOW !!!"
cat <<EOF
(RSA) SHA256:pPcBY4E33vwreETbz5KJUIzZpWWzaZPhrpnLaFa7WuQ
(ECDSA) SHA256:kg2Zaqpd8ZuluPzlpFS9rEw0KR1UmxD9jSG6+2tr28A

EOF
echo "THEN DOUBLE-CHECK WITH https://www.openbsd.org/anoncvs.html !!!"

cvs -qd anoncvs@anoncvs.au.openbsd.org:/cvs get -P src/etc/signify
cd src/etc/signify/
mv *.pub $here

cd $here

echo "VERIFY THAT ONLY NEW FILES WERE ADDED"
git status
read

sha256sum *.pub > keys.sha256
gpg --detach-sign --armor keys.sha256
